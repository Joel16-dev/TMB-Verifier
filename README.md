# TMB-Verifier

# How to install:

- Make sure python is installed.
- This script requires that you have Google Chrome installed and selenium (python lib) installed.
- If you don't have PIP: look into this (It will help you install PIP) -> https://pip.pypa.io/en/stable/installing/
- Then open CMD/powershell/terminal and use the following command to install selenium using PIP: ```pip install selenium```
- Also use the following command to install lxml using PIP: ```pip install lxml```
- Download the [TMB-Verifier-master.zip](https://gitlab.com/Joel16-dev/TMB-Verifier/-/archive/master/TMB-Verifier-master.zip)
- Open the TMB-Verifier-master folder and here open CMD/powershell/terminal (right click or shift + right click).
- Run the script by entering the command ```python TMB-Verifier.py```.

# How to use:
- First enter first name and last name in the terminal/CMD. 
- Allow the popup table to populate.
- Select the person who's data you'd like to dump using the press button. (Please wait at least 5 seconds before selecting a different person)
- Outputs will be stored in the ```output``` folder with in the following format -> ```name_license.txt```
- To enter a different name -> close the terminal/CMD and enter a name again.
