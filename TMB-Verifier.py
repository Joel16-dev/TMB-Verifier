from appJar import gui
from lxml import etree
from selenium import webdriver
from sys import platform
import os
import re
import time

filename = []


def clear_html_tags(raw_html):
    tags = re.compile('<.*?>')
    clear_text = re.sub(tags, '', raw_html)
    return clear_text


def find_between(s, first, last):
    try:
        start = s.rindex(first) + len(first)
        end = s.rindex(last, start)
        return s[start:end]
    except ValueError:
        return ""


def tmb_verifier_init():
    if platform == 'linux':
        return 'linux/chromedriver'
    elif platform == 'win32' or platform == 'cygwin':
        return 'windows/chromedriver.exe'
    elif platform == 'darwin':
        return 'mac/chromedriver'


def tmb_verifier_get_updated_html(xpath):
    # HTML from `<html>`
    browser.execute_script("return document.documentElement.innerHTML;")

    # HTML from `<body>`
    browser.execute_script("return document.body.innerHTML;")

    # HTML from element with some JavaScript
    # element = browser.find_element_by_xpath('//td[@valign="' + class_name + '"]')
    element = browser.find_element_by_xpath(xpath)
    browser.execute_script("return arguments[0].innerHTML;", element)

    # HTML from element with `get_attribute`
    # element = browser.find_element_by_xpath('//td[@valign="' + class_name + '"]')
    element = browser.find_element_by_xpath(xpath)
    return element.get_attribute('innerHTML')


def tmb_verifier_fix_table(data):
    fixed_table = find_between(data, 'style="color:Black;height:73px;border-collapse:collapse;">', '</table>')
    fixed_table = fixed_table[:-121]  # Get the data between the tags <table></table>
    fixed_table = re.sub('<td>\n', '', fixed_table)
    fixed_table = re.sub('</a>', '', fixed_table)
    fixed_table = re.sub('style="color:Blue;">', 'style="color:Blue;"></a><td>', fixed_table)
    fixed_table = re.sub('<a.*?</a>', '', fixed_table)
    fixed_table = re.sub('<tbody>', '', fixed_table)
    fixed_table = re.sub('&nbsp;', 'N/A', fixed_table)  # replace "&nbsp;" to N/A
    fixed_table = '<table>' + fixed_table + '</table>'
    return fixed_table


def tmb_verifier_dump_relevant_details(path, data):
    if os.path.exists('output/' + path):
        os.remove('output/' + path)

    with open('output/' + path, 'a+') as output_file:
        output_file.write(data)

    output_file.close()


def tmb_verifier_table_get_row(btn=None):
    # print(btn)

    selection: int = int(btn)
    selection = selection + 2

    # print(selection)

    if selection < 10:
        selected_id: str = "//a[contains(@href, 'ctl00$BodyContent$gvSearchResults$ctl0" + str(selection) + "$ctl00')]"
    else:
        selected_id: str = "//a[contains(@href, 'ctl00$BodyContent$gvSearchResults$ctl" + str(selection) + "$ctl00')]"

    # print(selected_id)
    select_item = browser.find_element_by_xpath(selected_id)
    select_item.click()

    new_html = tmb_verifier_get_updated_html('//td[@valign="top"]')

    new_html = re.sub('&nbsp;', '', new_html)
    new_html = clear_html_tags(new_html)
    new_html = re.sub(' +', ' ', new_html)
    new_html = re.sub('\t+', ' ', new_html)
    tmb_verifier_dump_relevant_details(filename[selection - 2], new_html)

    time.sleep(1)
    browser.execute_script("window.history.go(-1)")

    return True


if not os.path.exists('output'):
    os.makedirs('output')

chrome_driver = 'drivers/' + tmb_verifier_init()
os.environ["webdriver.chrome.driver"] = chrome_driver

browser = webdriver.Chrome(chrome_driver)
browser.get('https://public.tmb.state.tx.us/HCP_Search/searchinput.aspx')
time.sleep(1)
btn_accept = browser.find_element_by_id('BodyContent_btnAccept')
btn_accept.click()

first_name = browser.find_element_by_id('BodyContent_tbFirstName')
first_name.send_keys(input('First name: '))

last_name = browser.find_element_by_id('BodyContent_tbLastName')
last_name.send_keys(input('Last name: '))

btn_search = browser.find_element_by_id('BodyContent_btnSearch')
btn_search.click()

html = tmb_verifier_get_updated_html('//div[@class="BaseForm"]')
result_table = tmb_verifier_fix_table(html)

# print(result_table)

table = etree.XML(result_table)
rows = iter(table)
headers = [col.text for col in next(rows)]

desired_list = dict()

app = gui()
app.setBg("orange")
app.setFont(size=12, family="Verdana")

app.addTable("g1", [["Name", "License", "Type", "Address", "City"]], action=tmb_verifier_table_get_row)
for row in rows:
    values = [col.text for col in row]
    desired_list = dict(zip(headers, values))
    # print(desired_list)
    app.addTableRow("g1", [desired_list['Name'], desired_list['License'], desired_list['Type'],
                    desired_list['Address'], desired_list['City']])
    name = desired_list['Name'] + '_' + desired_list['License'] + '.txt'
    name = re.sub('\n +', '', name)
    name = re.sub(', ', '', name)
    name = re.sub(' ', '', name)
    filename.append(name)
    # print(filename)


cleared_html_data = clear_html_tags(re.sub('</td><td>', ' ', result_table))
# print(re.sub('NameLicenseTypeAddressCity', '', re.sub(' +', ' ', cleared_html_data)))

app.go()
